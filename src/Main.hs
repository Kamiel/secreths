module Main where

import Prelude hiding
    ( getLine
    , null
    , putStrLn
    , words)
import Control.Monad (unless)

import Data.ByteString (getLine)
import Data.ByteString.Lazy.Char8
    ( fromStrict
    , null
    , putStrLn
    , readInt
    , words)

import Estimation (solve)

main :: IO ()
main = do
    s <- getLine
    t <- return $ fromStrict s
    unless (null t) $ do
        printSolution t
        main
  where
    printSolution = putStrLn . solve . testCase
    testCase c    = toPrices $ map readInt $ words c
    toPrices      = flip foldr [] $ \m ps -> case m of
                                              Nothing -> ps
                                              Just p  -> fst p : ps
